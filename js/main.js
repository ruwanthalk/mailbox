/**
 * 
 * @author Ruwantha Lankathilaka
 * @description This file contains helper methods for the to the mail box page
 */

"use strict";


//+++++++++++++++++++++++++++ INBOX SECTION ++++++++++++++++++++++++++++++++++++
/**
 * @description inbox object is related to the all the functions on inbox area
 */
var inbox = new Object();

/**
 * @param {type} tableId Id of the table that want to get the number of children
 * @returns {jQuery.length} integer | undefined
 */
inbox.getCount = function(tableId){
    return $("#inboxTable").find("tr").length;
};

/**
 * @description get the unreaded message count
 * @returns {jQuery.length} integer | undefined
 */
inbox.getNewMessagesCount = function(){
    return $("#inboxTable").find("tr.unread").length;
};

/**
 * @description Get the stared email count
 * @returns {_$.length|$.length} integer | undefined
 */
inbox.getStaredCount = function(){
    return $("#inboxTable tr .fa-star.text-yellow").length;
};

/**
 * @description description
 * @returns {_$.length|$.length}
 */
inbox.getImportentCount = function(){
    return $("#inboxTable tr td .label-danger").length;
};

/**
 * @description Give the promotion count
 * @returns {_$.length|$.length} integer | undefined
 */
inbox.getPromotionCount = function(){
    return $("#inboxTable tr td .label-warning").length;
};

/**
 * @description Give the social meadia related email count
 * @returns {_$.length|$.length} integer | undefined
 */
inbox.getSocialCount = function(){
    return $("#inboxTable tr td .label-info").length;
};

/**
 * @description Give the top email senders name-count array based on parameters
 * @param {type} topCount number of top senders
 * @returns {undefined} array of names-count of top senders | false
 */
inbox.getTopSenders = function(topCount){
    
    var nameCountMap = [];
    var counter = 0;
    var topSenders = [];
    var tempCountAr = [];
    
    
    if(topCount === null){
        topCount = 4;
    }
    //Select td and get the anchor elements to loop
    $("#inboxTable tr td.mailbox-name a").each(function(i,e){
        var senderName = $(e).html();
        if( senderName in nameCountMap){
            
            nameCountMap[senderName] = ++nameCountMap[senderName];
        }else{
            nameCountMap[senderName] = 1;
        }
    });
    
    //Get cumilative counts for sorting
    for(var i in nameCountMap){
        tempCountAr.push(nameCountMap[i]);
    }
    //sort counts descending order
    tempCountAr.sort(function(a, b){return b-a;});
    
    //Create name-count array that matched for the sorted counts
    for(var i = 0 ; i < topCount; i++){//get top 3
        for(var key in nameCountMap){// lop all name counts
            if(nameCountMap[key] === tempCountAr[i]){
                topSenders[key] = nameCountMap[key]; 
            }
        }
    }
    return topSenders;
};

/**
 * @description Highlight the top senders on hover
 * @param {type} element hovering element (tr)
 * @param {type} numberOfTopSenders Configuration of how many top senders should
 * highlight
 * @returns {Boolean} true - if highlighted else false
 */
inbox.highlightTopSeners = function(element,numberOfTopSenders){
    
    var topSenders = [];
    var topSendersNames;
    
    if(numberOfTopSenders !== undefined){
        numberOfTopSenders = 4;
    }
    
    topSenders = inbox.getTopSenders(numberOfTopSenders);
    
    //if there is no top senders
    if(Object.keys(topSenders).length < 1){//** compare the js object legth
        
        return false;
    }
    
    topSendersNames = [];
    for(var key in topSenders){
        topSendersNames.push(key);
    }
    
    //should compared against -1 not 0 as 0 can first elemenet has index of 0
    if($.inArray($(element).find("td.mailbox-name a").html(),topSendersNames)> -1){
        $(element).addClass("highlight");
        return true;
    }
    return false;
};

/**
 * @description This methord will take the tr element which want its highlighted text
 * child to be removed and return the lable class if the element is removed else ruturn false
 * @param {type} element : the parant element which want to be remove its child
 * @returns {Boolean} Returns the tag if exist else false
 */
inbox.removeHighlight = function(element){
    var span;
    var label = false;
    element = $(element).closest("tr");
    if(($(element).find(".mailbox-name").children("span").length === 1 ) // has span
//          &&  $(element).find(".mailbox-name span").hasClass("label-danger") // as label-danger
          ){
        span =  $(element).find(".mailbox-name span");
        if($(span).hasClass("label-danger") === true){
            label = "label-danger";
        }else if($(span).hasClass("label-warning") === true){
            label = "label-warning";
        }else if($(span).hasClass("label-info") === true){
            label = "label-info";
        }
        $(element).find(".mailbox-name span").remove();
    } 
    return label;
};

/**
 * @description Search the sender and the subject line based on the search box value
 * @param {type} searchText any string value
 * @returns {undefined} void
 */
inbox.search = function (searchText){
 
    $("#inboxTable tr").each(function(){
    
        //found in name
        if($(this).find(".mailbox-name a").text().toUpperCase()
                .includes(searchText.toUpperCase())){
            $(this).removeClass('hidden');
        }
        //found in subject
        else if($(this).find(".mailbox-subject").text()
                .toUpperCase().includes(searchText.toUpperCase())){
            $(this).removeClass('hidden');
        }else{
            $(this).addClass('hidden');
        }
    });
};


/**
 * @description Select all and select non functionality
 * @param {type} btn ckecked button element
 * @returns {undefined} void
 */
inbox.toggleSelectAll = function (btn){
  
    var isChecked;
    
    //Need to uncheck
    if($(btn).find("i").hasClass("fa-check-square-o")){
        $(".mailbox-controls button.checkbox-toggle").find("i").removeClass("fa-check-square-o");
        $(".mailbox-controls button.checkbox-toggle").find("i").addClass("fa-square-o");
        isChecked = false;
    }
    //Need to check
    else{
        $(".mailbox-controls button.checkbox-toggle").find("i").removeClass("fa-square-o");
        $(".mailbox-controls button.checkbox-toggle").find("i").addClass("fa-check-square-o");
        isChecked = true;
    }
    
    $('#inboxTable tr').each(function(){
        if(isChecked === true){
            $(this).first().find("input:checkbox").prop("checked",true);
        }else{
            $(this).first().find("input:checkbox").prop("checked",false);
        }
        
    });  
};

/**
 * @description Delete the rows selected,
 * Action must remove/hide rows from the table and update the UI label 'Trash' 
 * @returns {undefined} void
 */
inbox.deleteSelected = function(){
    var isDeleted = false;
    var importent = 0,promotion = 0,social = 0;
    var topSendersCount = [];
    var tempSenderCount = 0;
    var senderName;
    var totoalDeletions = 0;
    
    //Get tag count for each to be deleted
    $("#inboxTable tr").each(function () {
        if($(this).find("td input").prop("checked")){
            if($(this).find("td.mailbox-name span").hasClass('label-danger')){
                importent++;
            }else if($(this).find("td.mailbox-name span").hasClass('label-warning')){
                promotion++;
            }else if($(this).find("td.mailbox-name span").hasClass('label-info')){
                social++;
            }
            
            if($(this).data('isDeleted') !== true){// not already deleted(hidden)
                $(this).data('isDeleted',true);
                $(this).addClass('hidden');
                isDeleted = true;
                //add top sender counts to deduct
                senderName = $(this).find("td.mailbox-name a").text();
                if( senderName in topSendersCount){
                    tempSenderCount = topSendersCount[senderName];
                    tempSenderCount++;
                    topSendersCount[senderName] = tempSenderCount;
                }else{
                    topSendersCount[senderName] = 1;
                }
                totoalDeletions++;
            }else{// alredy deleted
                    
            }
        }
    });
    //if at least one deleted
    if(isDeleted){
        //update delete icon
        var trashBtn = $(".mailbox-controls .btn-group").children(":first-child").find("i");
        trashBtn.removeClass(".fa-trash-o").addClass("fa-trash");
        
        //deduct respective tag numbers
        if(importent > 0){
            box.deductTagCount("label-danger",importent);
        }else if(promotion > 0){
            box.deductTagCount("label-warning",promotion);
        }else if(social > 0){
            box.deductTagCount("label-info",social);
        }
        //deduct top senders
        box.deductTopSenders(topSendersCount);
        
        //deduct folder count
        box.deductInboxCount(totoalDeletions);
        //increase the trash count
        box.increaseTrashCount(totoalDeletions);
    }
};


/**
 * @description All marked rows will be removing the "unread" class
 * @returns {undefined} void
 */
inbox.markAsRead = function(){
    
    $("#inboxTable tr").each(function(){
        if($(this).children(":first-child").find("input").prop("checked")){
            $(this).removeClass("unread");
        }
    });
    
};

/**
 * @description Mark as read the selected item
 * @returns {undefined} void
 */
inbox.markAsUnread = function (){
    $("#inboxTable tr").each(function(){
       if($(this).children(":first-child").find("input").prop("checked")){
           $(this).addClass("unread");
       }
    });
};

/**
 * @description Render the message table based on the clicks on folder section
 * Show deleted items, inbox and other sections as functional
 * @param {type} element Clicked folder element
 * @returns {undefined} void
 */
inbox.navigateFolders = function(element){
    
    //Message box body change logic - render the body according to the click
    if($(element).data('folder') === 'Inbox'){
        
        $('.box-primary > .box-header > h3').text('Inbox');
        
        //show the tbody if hidden (Comback from other tabs)
        if($("#inboxTable tbody").hasClass('hidden') === true){
            $("#inboxTable tbody").removeClass("hidden");
        }
        
        //show undeleted messages
        $("#inboxTable tbody tr").each(function (){
            if($(this).data('isDeleted') !== true){//show not deleted
                $(this).removeClass('hidden');
            }else{
                $(this).addClass('hidden');
            }
        });
    }
    else if($(element).data('folder') === 'Sent'){
        $('.box-primary > .box-header > h3').text('Sent');
        $("#inboxTable tbody").addClass('hidden');
    }
    else if($(element).data('folder') === 'Draft'){
        $('.box-primary > .box-header > h3').text('Draft');
        $("#inboxTable tbody").addClass('hidden');
    }
    else if($(element).data('folder') === 'Junk'){
        $('.box-primary > .box-header > h3').text('Junk');
        $("#inboxTable tbody").addClass('hidden');
    }
    else if($(element).data('folder') === 'Trash'){
        
        $('.box-primary > .box-header > h3').text('Trash');
        
        //Unhide if the body is hidden by clicking other tabs
        if($("#inboxTable tbody").hasClass('hidden') === true){
            $("#inboxTable tbody").removeClass("hidden");
        }
        //show deleted messages
        $("#inboxTable tbody tr").each(function (){
            if($(this).data('isDeleted') === true){//show only deleted
                $(this).removeClass('hidden');
            }else{
                $(this).addClass('hidden');
            } 
        });
    }
};






//+++++++++++++++++++++++++++ ACCORDIAN SECTIONS +++++++++++++++++++++++++++++++


var box = new Object();

/**
 * @description upone clicking on the accordian + or - toggle the section
 * @param {type} element clicked element
 */
box.accordianToggle = function(element){
    $(element).parents(".box-solid").find(".box-body").toggle(650);
    
    if($(element).find(".fa-minus").hasClass("fa-minus")){
        $(element).find(".fa-minus").toggleClass("fa-plus fa-minus");
    }else{
        $(element).find(".fa-plus").toggleClass("fa-minus fa-plus");
    }
};

/**
 * 
 * @description description {type} label Upone the clicking on the tagged mail the corresponding tag
 * section will be deducted the count
 * @param {string} clicked tag label
 * @param {integer} count to deduct from existing count 
 * @returns {Boolean} true if tag exist and count is deducted else false
 */
box.deductTagCount = function(label,decrement){
    var count = $("#tagBox").find("."+label).html();
    
    if( (count % 1 === 0) && count > 0){// if count is integer and not zero
        count = count - decrement;
        $("#tagBox").find("."+label).text(count);
        return true;
    }
    return false;
};

/**
 * @description Set the active class accordint to the clicks on folder section
 * @param {type} element data-folder attributed element to chatch the click event
 * @returns {undefined} void
 */
box.navigateFolders = function (element){
    
    //remove current active class
    $(element).closest('ul').each(function(){
        $(this).find('li').removeClass('active');
    });
    //mark as active
    $(element).closest('li').addClass('active');
    
    inbox.navigateFolders(element);
    
};

/**
 * @description Deduct the top senders count. Perform bulk operations rather than
 * individual iteration
 * @param {array} nameCount name-count map array to bulk deduction from top senders 
 * @returns void
 */
box.deductTopSenders = function(nameCount){
    var tempCount;
    var tempName;
    $("#topSenders li").each(function () {
        tempName = $(this).find("a span").first().text();
        if( tempName in nameCount){
           tempCount = $(this).find("a span:nth-child(2)").text();
           $(this).find("a span:nth-child(2)").text((tempCount - nameCount[tempName]));
       } 
    });
    
};

/**
 * @description Deduct the inbox count when messages deletes
 * @param {integer} deductBy - count to deduct the inbox
 * @returns void
 */
box.deductInboxCount = function (deductBy){
    var currentCount = $("#inboxCount").text();
    if((currentCount-deductBy) > 0){
        $("#inboxCount").text(currentCount - deductBy);    
    }
};

box.increaseTrashCount = function (count){
//    console.log(count)
    var currentValue = parseInt($("#trashFolder a span:nth-child(3)").text());
    console.log(currentValue)
    $("#trashFolder a span:nth-child(3)").text(currentValue+count);
    
};